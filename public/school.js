(function() {
    "use strict";

    angular.module('school', ['ngRoute']);

    angular.module('school').config(Routes);

    function Routes($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                redirectTo: '/classes'
            }).when('/classes', {
                templateUrl: 'class_list.html',
                controller: 'ClassListController'
            }).when('/class/:id', {
                templateUrl: 'class.html',
                controller: 'ClassController'
            }).when('/students', {
                templateUrl: 'student_list.html',
                controller: 'StudentListController'
            }).when('/student/:id', {
                templateUrl: 'student.html',
                controller: 'StudentController'
            });
        $locationProvider.html5Mode(false).hashPrefix('!');
    }

    angular.module('school').factory('schoolService', SchoolService);

    function SchoolService($http) {
        return {
            getClasses: function (name) {
                var params = {};
                if (name) {
                    params['name'] = name;
                }
                return $http({
                    method: 'GET',
                    url: '/api/classes',
                    params: params
                });
            },
            getClass: function (id) {
                return $http({
                    method: 'GET',
                    url: '/api/classes/' + id
                });
            },
            getStudent: function (id) {
                return $http({
                    method: 'GET',
                    url: '/api/students/' + id
                });
            },
            getStudents: function (first_name, last_name) {
                var params = {};
                if (first_name) {
                    params['first_name'] = first_name;
                }
                if (last_name) {
                    params['last_name'] = last_name;
                }
                return $http({
                    method: 'GET',
                    url: '/api/students/',
                    params: params
                });
            }
        };
    }

    angular.module('school').controller('ClassListController', ClassListController);

    function ClassListController($scope, $routeParams, $location, $timeout, schoolService) {
        $scope.list = [];
        schoolService.getClasses().then(function(result) {
            if (result.status == 200) {
                $scope.list = result.data;
            }
        });
    }

    angular.module('school').controller('ClassController', ClassController);

    function ClassController($scope, $routeParams, $location, $timeout, schoolService) {
        $scope.obj = {};
        schoolService.getClass($routeParams.id).then(function(result) {
            if (result.status == 200) {
                $scope.obj = result.data;
            }
        });
    }

    angular.module('school').controller('StudentListController', StudentListController);

    function AverageGpa(index, student) {
        var sum = 0, i, gpa;
        for (i = 0; i < student.classes.length; i++) {
            gpa = parseFloat(student.classes[i]._pivot_gpa);
            sum += gpa;
        }
        student.average_gpa = (sum/student.classes.length).toFixed(1);
    }

    function StudentListController($scope, $routeParams, $location, $timeout, schoolService) {
        $scope.students = [];
        $scope.first_name = "";
        $scope.last_name = "";
        $scope.search = function() {
            schoolService.getStudents($scope.first_name, $scope.last_name).then(function(result) {
                if (result.status == 200) {
                    $.each(result.data, AverageGpa);
                    $scope.students = result.data;
                }
            });
        };
        $scope.search();
    }

    angular.module('school').controller('StudentController', StudentController);

    function StudentController($scope, $routeParams, $location, $timeout, schoolService) {
        $scope.student = {};
        schoolService.getStudent($routeParams.id).then(function(result) {
            if (result.status == 200) {
                AverageGpa(0, result.data);
                $scope.student = result.data;
            }
        });
    }

})();
