# Full Stack Developer

## Candidate Test Assignment

The goal of the assignment is to create a back-end web service and a user interface for searching a database. The details of the project are as follows.

We need an application that will allow us to search for students by their name (first, last, or both). The search results should return the following fields:

* First Name
* Last Name
* Average Grade (GPA)

From the student search, the user should be able to acquire more information about the student via a details view. The details should include the following fields:

* First Name
* Last Name
* Email Address
* Average Grade (GPA)

A list of classes that the student is attending along with the grade that the student has in each of the classes.

The front end of the app should be coded using HTML, CSS, and Javascript. 

The back end of the application should be coded using a JVM based language/Python/Ruby/Node. The back-end should return JSON.

