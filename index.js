
var knex = require('knex')({
    client: 'pg',
    connection: {
        host     : '127.0.0.1',
        user     : 'headmaster',
        password : 'discipline',
        database : 'school',
        charset  : 'utf8'
    }
});

var bookshelf = require('bookshelf')(knex);

var Student = bookshelf.Model.extend({
    tableName: 'students',
    classes: function() {
        return this.belongsToMany(Class).withPivot(['gpa']).through(StudentClass);
    }
});

var StudentClass = bookshelf.Model.extend({
    tableName: 'student_class',
    student: function() {
        return this.belongsTo(Student);
    },
    'class': function() {
        return this.belongsTo(Class);
    }
});

var Class = bookshelf.Model.extend({
    tableName: 'classes',
    students: function() {
        return this.belongsToMany(Student).through(StudentClass);
    }
});

var Boom = require('boom');

var Path = require('path');

var Hapi = require('hapi');

var Inert = require('inert');

var server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo:  Path.join(__dirname, 'public')
            }
        }
    }
});

server.connection({
    host: '0.0.0.0',
    port: 3000
});

server.register({
    register: Inert
}, function (err) {
    if (err) {
        console.log('failed loading inert');
    }
});

function wildcard(text) {
    return '%' + text + '%';
}

server.route({
    method: 'GET',
    path: '/api/classes/{id?}',
    handler: function (request, reply) {
        if (request.params.id) {
            new Class({id: request.params.id})
                .fetch({withRelated: ['students']})
                .then(function(model) {
                    if (model) {
                        reply(model);
                    } else {
                        reply(Boom.notFound());
                    }
                });
        } else {
            var classes = new Class();
            if (request.query) {
                if (request.query.name) {
                    students.query('where', 'name', 'LIKE', wildcard(request.query.name));
                }
            }
            reply(classes.fetchAll());
        }
    }
});

server.route({
    method: 'GET',
    path: '/api/students/{id?}',
    handler: function (request, reply) {
        if (request.params.id) {
            new Student({id: request.params.id})
                .fetch({withRelated: ['classes']})
                .then(function (model) {
                    if (model) {
                        reply(model);
                    } else {
                        reply(Boom.notFound());
                    }
                });
        } else {
            var students = new Student();
            if (request.query) {
                if (request.query.first_name) {
                    students.query('where', 'first_name', 'LIKE', wildcard(request.query.first_name));
                }
                if (request.query.last_name) {
                    students.query('where', 'last_name', 'LIKE', wildcard(request.query.last_name));
                }
            }
            reply(students.fetchAll({withRelated: ['classes']}));
        }
    }
});

server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: '.',
            redirectToSlash: true,
            index: true
        }
    }
});

server.on('internalError', function (request, err) {
    console.log(err.data.stack);
})

server.start(function() {});

