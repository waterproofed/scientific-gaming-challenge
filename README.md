
# school

this is the solution to the scientific gaming programming challenge

## installation

### source code

the solution was tested on node v0.10.28 compiled from source on ubuntu 12.04 LTS

```bash

    git clone git@bitbucket.org:waterproofed/scientific-gaming-challenge.git

    cd scientific-gaming-challenge

    npm install

```

### postgresql

the solution was tested on postgresql 9.4 on ubuntu 12.04 LTS

```bash

    sudo -u postgres psql

```

```sql

    CREATE DATABASE school;

    CREATE USER headmaster WITH PASSWORD 'discipline';

    GRANT CONNECT ON DATABASE school to headmaster;

    \connect school;

    CREATE TABLE students (id serial, first_name varchar(128) null, last_name varchar(128) null, email varchar(128) null, constraint pk_students primary key (id));

    CREATE TABLE classes (id serial, name varchar(128) not null, constraint pk_classes primary key (id));

    CREATE TABLE student_class (id serial, student_id int not null, class_id int not null, gpa numeric(2,1) not null, constraint pk_student_class primary key (student_id, class_id), constraint uk_student_class unique (id), constraint fk_student_class__student foreign key (student_id) references students (id), constraint fk_student_class__class foreign key (class_id) references classes(id));

    GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO headmaster;

    COPY students (first_name, last_name, email) FROM '/srv/projects/school/data/students.csv' WITH CSV HEADER;

    COPY classes (name) FROM '/srv/projects/school/data/classes.csv' WITH CSV HEADER;

    INSERT INTO student_class (student_id, class_id, gpa) SELECT generate_series AS student_id, id AS class_id, random()*5 AS gpa FROM (SELECT id, generate_series, ROW_NUMBER() OVER(PARTITION BY generate_series ORDER BY RANDOM()) AS r FROM classes CROSS JOIN generate_series(1,50)) t WHERE r < random()*10;

```

## running

```bash

    node ./index.js

```

The web-server is running on

    http://localhost:3000/

